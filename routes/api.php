<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

 Route::get('get_image/{category}/{year}/{month}/{name}.{ext}/{dimention}', 'ImageController@index');

Route::group(['namespace' => 'Api\Frontend'], function () {
    require(__DIR__ . '/Routes/Api/api_frontend_controls.php');
});


   

