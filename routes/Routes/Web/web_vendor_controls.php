<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/signout', 'AuthController@signout_func');
Route::group(['middleware' => ['guest:admin', 'revalidate']], function () {
    Route::get('/', 'AuthController@index');
    Route::get('/register', 'AuthController@get_register');
    Route::get('/login', function () {
        return redirect('/superadmin/');
    });
    Route::post('/auth-login', 'AuthController@authlogin_func');
});

/*
|--------------------------------------------------------------------------
| Users Control routes
|
*/
Route::group(['middleware' => ['admin', 'revalidate']], function () {
    Route::get('dashboard', 'DashboardController@index');
    Route::get('settings', 'UserController@moderators');
    Route::get('school-info', 'DashboardController@school_info');

    Route::get('get_image/{category}/{dirYear}/{dirMonth}/{image_name}_{image_size}.{image_ext}', 'ImageController@index');

  
    Route::group(['prefix' => 'lawn'], function () {
        
          Route::get('/booking_details/{id}', 'LawnController@booking_details');
           Route::get('/display-booking-detailes/{id}', 'LawnController@get_details');

           Route::post('/booking-cancel', 'LawnController@booking_cancel');

                 Route::get('booking/{id}', 'LawnController@index');
                   Route::get('/add-ground', 'LawnController@get_add_details');
                 Route::post('/add/{ground_id}', 'LawnController@add');
                 Route::post('/update/{id}', 'LawnController@update');
                 // Route::get('/display-ground-detailes', 'LawnController@get_details');
                 Route::get('/edit/{id}', 'LawnController@edit');
                 Route::delete('delete/{id}', 'LawnController@delete');


             });

    Route::group(['prefix' => 'change-pass'], function () {
        Route::get('/', 'UserController@get_change_pass');
        Route::post('/', 'UserController@change_pass');
    });
});



/*
|--------------------------------------------------------------------------
| Post Controls
|
*/
