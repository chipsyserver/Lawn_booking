<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:superadmin', ['except' => 'signout_func']);
    }

    public function index()
    {
        return view('Admin.login');
    }

    public function get_register()
    {
        return view('Backend.register');
    }
    

    public function authlogin_func(Request $request) {
        
        $inputs    = $request->all();
        $rules     = array(
            'username' => 'required|max:255',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $creds = [
            'user_name'          => $data['username'],
            'password'           => $data['password'],
            'role_id'            =>1000
        ];


        if (Auth::guard('superadmin')->attempt($creds,true)) {
             return response()->json(array(
                'success' => true,
                'message' => "Login successfully completed"
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => "Invalid email or password"
            ));
        }

        return response()->json(array(
            'success' => false,
            'message' => "Invalid email or password"
        ));
    }
    
    
    public function signout_func()
    {
        Auth::guard('superadmin')->logout();
        return redirect('/superadmin/');
    }
}
