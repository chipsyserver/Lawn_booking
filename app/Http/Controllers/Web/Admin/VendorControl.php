<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use App\User;
use Auth;
use App\Models\Vendor;
use DateTime;

class VendorControl extends Controller
{

     public function index(Request $request){
     return view('Admin.pages.user');
    }

    public function add(Request $request){
    $rules     = array(
            'username' => 'required',
            'email' => 'required'
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
        $roleId= 1001;
         
           $user = Vendor::where('email', '=', $request->email)->first();
      if ($user === null) {
  
          $user = new User;
                $n_password = Hash::make($request->new_password);
                $user->user_name=$request->username;
                $user->password = $n_password;
                $user->email=$request->email;
                 $user->mobile_num=$request->phone;
                $user->role_id=$roleId;
                $user->save();
                return response()->json(array(
                'success' => true,
                'message' => 'Success', ));
                }

            else
                {
                return response()->json(array(
                'success' => false,
                'message' => 'Email ID already Present', ));
              }


  }


  public function update(Request $request,$id)
  {
    $rules     = array(
            'username' => 'required',
            'email' => 'required'
            
            
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $data      = $request->all();

        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }
                    $user =User::where('id', $id)->first();
           
              $user->user_name=$request->username;
              if($request->new_password!="********")
              {
                 $n_password = Hash::make($request->new_password);
                 $user->password = $n_password;
              }
                
                $user->mobile_num=$request->phone;
                $user->email=$request->email;
                $user->save();

                  return response()->json(array(
                'success' => true,
                'message' => 'successfully updated',));
   
         
           

  }

      public function user_details(Request $request){
        $roleId=1001;
        $users = Vendor::orderBy('created_at', 'DESC')->paginate(10);
        foreach ($users as $user) {
        $user->registered_on = date('d-m-Y', strtotime($user->created_at));
        }
       return $users;
        
       }

      public function edit(Request $request, $id){

              $user =Vendor::where('id', '=', $id)->first();
              $user->password_str="********";
              return $user;
           }



      public function delete(Request $request){
          $id=$request->id; 
          $user =Vendor::where('id',$id)->first();
          $user->delete();
             return response()->json(array(
             'success'=>"true",
             'message' => " successfully delete"));
      }
}