<?php
namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\User;
use Illuminate\Http\Request;
use Validator;
use DB;
use Auth;
use View;
use Hash;
class UserController extends Controller{
   
   

    public function get_change_pass(Request $request){
       
        return view('Admin.pages.settings');
    }


     public function change_settings(Request $request){
        $rules     = array(
            'username' => 'required',
            'current_password' => 'required'
        );
        
        $change_pass = @$request->all()['change_pass'];

        if (@$request->all()['change_pass']) {
            $rules['new_password'] = 'required';
            $rules['confirm_password'] = 'required|same:new_password';
        }

        $messages = [
            'confirm_password.same' => 'Confirm Password should match the New Password',
            'confirm_password.required' => 'Confirm Password is required',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

      
        if (Hash::check($data['current_password'], Auth::guard('superadmin')->user()->password)) {
            $user = Admin::find(Auth::guard('superadmin')->user()->id);

            if ($change_pass) {
                $n_password = Hash::make($data['new_password']);
                $user->password = $n_password;
            }

            $user->user_name = $data['username'];
            $user->save();
            // Auth::logout();
            // Auth::loginUsingId(Auth::id());

            return response()->json(array(
                'success' => true,
                'message' => 'Success',
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => 'Current password entered is incorrect',
            ));
        }
    }


    // public function change_pass(Request $request)
    // {
    //     $rules     = array(
    //         'oldPass' => 'required',
    //         'newPass' => 'required',
    //         'rePass' => 'required|same:newPass'
    //     );

    //     $messages = [
    //         'oldPass' => 'Current password is required',
    //         'rePass.same' => 'Mismatch in New password and Repeat Password',
    //         'newPass.required' => 'New password is required',
    //         'rePass.required' => 'Repeat password is required',
    //     ];

    //     $data      = $request->all();

    //     if (isset($data['task']) && $data['task'] == 'old_pass_verify') {
    //         return [
    //             'status' => ((Hash::check($data['oldPass'], Auth::guard('admin')->user()->password)) ?  'OK' : 'ERROR')
    //         ];
    //     } else {
    //         $validator = Validator::make($request->all(), $rules, $messages);
            
    //         $valArr = array();

    //         foreach ($validator->getMessageBag()->toArray() as $key => $value) {
    //             $errStr = $value[0];
    //             array_push($valArr, $errStr);
    //         }

    //         if (!empty($valArr)) {
    //             $errStrFinal = implode(',', $valArr);
    //         }

    //         if ($validator->fails()) {
    //             return response()->json(array(
    //                 'success' => false,
    //                 'message' => $errStrFinal
    //             ));
    //         }
            
    //         $n_password = Hash::make($data['newPass']);
    //         $res = false;

    //         if (Hash::check($data['oldPass'], Auth::guard('admin')->user()->password)) {
    //             $admin = Auth::guard('admin')->user();
    //             $admin->password = $n_password;
    //             $res = $admin->save();
    //         }

    //         if ($res) {
    //             Auth::guard('admin')->loginUsingId(Auth::guard('admin')->user()->id);

    //             return response()->json(array(
    //                 'success' => true,
    //                 'message' => 'Updated successfully.'
    //             ));
    //         } else {
    //             return response()->json(array(
    //                 'success' => false,
    //                 'message' => 'Current Password is Wrong.'
    //             ));
    //         }
    //     }
    // }

    public function moderators(Request $request)
    {
    }
}
