<?php
namespace App\Http\Controllers\Web\Vendor;

use App\Http\Controllers\Controller;
use App\Models\Vendor;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => 'signout_func']);
    }

    public function index()
    {
        return view('Vendor.login');
    }

    public function get_register()
    {
        return view('Backend.register');
    }

    public function authlogin_func(Request $request)
    {
        // echo 'ddd';die;
        $inputs    = $request->all();
        $rules     = array(
            'email' => 'required|max:255',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

        $creds = [
            'email'          => $data['email'],
            'password'       => $data['password'],
            'role_id'        => '1001'
        ];
      

        if (Auth::guard('admin')->attempt($creds,true)) {
            // die(json_encode(Auth::guard('superadmin')->user()));
        
            return response()->json(array(
                'success' => true,
                'message' => "Login successfully completed"
            ));
        } else {
            return response()->json(array(
                'success' => false,
                'message' => "Invalid username or password"
            ));
        }

        return response()->json(array(
            'success' => false,
            'message' => "Invalid email or password"
        ));
    }
    
    
    public function signout_func()
    {
        Auth::guard('admin')->logout();
        return redirect('/vendor');
    }
}
