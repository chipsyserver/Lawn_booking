<?php
namespace App\Http\Controllers\Web\Vendor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use DB;
use Hash;
use Auth;
use App\Booking_detailes;
use App\Ground_detailes;
use App\Image_master;
use App\User;
use DateTime;
use Image;


class LawnController extends Controller
{


public function index(Request $request)
    {

    return view('Vendor.pages.grounddetails');
       
    }

    public function  booking_details($id){

    $data=Booking_detailes::where('status',0)->where('ground_id',$id)->get();
    $events=array();
    foreach ($data  as $value) {
      $ev['title']='booking';
      $ev['start']=$value->booking_date;
      $ev['id']=$value->id;
      $events[]=$ev;
    }
   
    die(json_encode($events));

   }



     public function edit($id) {
     $data =Ground_detailes::find($id);
     $img=Image_master::where('ground_id',$data->id)->where('category','ground_images')->get();
     foreach ($img as $imgpath ) {
     $imgpath->url='/get_image/'.$imgpath->category.'/'.$imgpath->year.'/'.$imgpath->month.'/'.$imgpath->image_name.'.'.$imgpath->ext.'/300~200.png';
     unset($imgpath->ground_id,$imgpath->category,$imgpath->year,$imgpath->month,$imgpath->image_name,$imgpath->ext,$imgpath->created_at,$imgpath->updated_at);
     }
     $data->image=$img;
    return view('Vendor.pages.editground', ['ground' => $data]);
     }


     public function get_details(Request $request,$id){
       $data =Booking_detailes::where('booking_date',$request->date)->where('ground_id',$id)->where("status",0)->first();
     

       if($data){
       $user=User::find($data->user_id);
       // die(json_encode($user));
       if($user){
       $data->username=($user->user_name)?$user->user_name:$user->full_name;
 
       }
        return response()->json(array(
                    'success' => true,
                    'message' => "Details Is found.",
                    'data'=>$data
                    
                    ));
     
       }else {
            return response()->json(array(
                    'success' => false,
                    'message' => "Details Is not found."
                    
                    ));
        
       }
     }


     public  function booking_cancel(Request $request) {
      $res=Booking_detailes::find($request->id);
      if($res){
      $res->status=1;
      $res->save();
      return response()->json(array(
                    'success' => true,
                   'message' => "booking Cancelled successfully."
              
     ));
    }
    else{
      return response()->json(array(
                    'success' => false,
                    'message' => "Some Thing Happens"
                    
                    ));
    }
     
     }

     public function add(Request $request,$ground_id) {
     // die(json_encode("value"));
       $inputs    = $request->all();
        $rules     = array(
           'date' => 'required',
           

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

     
        $data =Ground_detailes::find($ground_id);
        $res=new Booking_detailes;
        $res->ground_id=$ground_id;
        $res->booking_date=$request->date;
        $res->price=$data->priceperday;
        $res->description=$request->desc;
        $res->user_id=Auth::guard('admin')->user()->id;
        $res->save();

          // $data = new Ground_detailes;
          // $data->title=$request->title;
          // $data->latitude=$request->latitude;
          // $data->best_suited=$request->best_suited;
          // $data->priceperday=$request->price_ground;
          // $data->description=$request->description;
          // $data->user_id=$request->user;
          // $data->longitude=$request->longitude;
          // $data->save();
          
 



         return response()->json(array(
                    'success' => true,
                    'message' => "Ground  Added successfully."
                    
                    ));
     }



    


      public function update(Request $request,$id) {
     // die(json_encode("value"));
       $inputs    = $request->all();
        $rules     = array(
            'date_booking' => 'required'

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

          $data =Ground_detailes::find($id);
         
          $res=new Booking_detailes;

          $res->ground_id=$id;
          $res->booking_date=$request->date_booking;
          $res->price=$data->priceperday;
          $res->user_id=4;
          $res->save();
        
         
  
         
    return response()->json(array(
    'success' => true,
    'message' => "Ground  Updated successfully."
     ));
     



    }

    public function delete($id){
    	$data =Ground_detailes::find($id);
    	$data->status=1;
        $data->save();

    	if($data){
    		return response()->json(array(
                    'success' => true,
                    'message' => "Floor  Delete successfully."));
    	}

    }

}