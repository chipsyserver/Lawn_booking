<?php
namespace App\Http\Controllers\Web\Website;
use App\Http\Controllers\Controller;
use Image;


class ImageController extends Controller
{
    
       public function index($category,$year, $month,$image_name,$ext,$dimention)
    {
    	
       
	  $remoteImage = storage_path() . "/Images/" .$category. "/" . $year . "/" . $month ."/" . $image_name."." . $ext;

	  
		
		$splitName = explode('~',$dimention);
	$splitName1 = explode('.',$splitName[1]);
	
	
		if(file_exists($remoteImage)){

			$img=Image::make($remoteImage)->resize($splitName[0], $splitName1[0]);
			$imginfo = getimagesize($remoteImage);
			
			header("Content-type: {$imginfo['mime']}");

			 return $img->response($splitName1[1]);
		}else {
			die(json_encode("Image Not found"));
		}
    }
	
	
}
