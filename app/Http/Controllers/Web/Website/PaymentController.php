<?php
namespace App\Http\Controllers\Web\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class PaymentController extends Controller
{
    public function index()
    {
        return view('website.pages.pay_now');
    }
}
