<?php
namespace App\Http\Controllers\Web\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Image_master;
use App\Ground_detailes;
use App\Booking_detailes;
use Validator;
Use DB;
Use Auth;
use Session;

class HomeController extends Controller
{

    public function pay_now(Request $request,$amount) {
       return view('website.pages.paynow',['amount'=>$amount]);
    }

    public function pay_success(Request $request) {
  
        return view('website.pages.success');
    }

    public function post_booking(Request $request) {

 	   // die(json_encode(Auth::user()->id));
       $inputs    = $request->all();
        $rules     = array(
           'date_of_booking' => 'required',
           

        );
        $validator = Validator::make($request->all(), $rules);
        $data      = $request->all();
        if ($validator->fails()) {
            return response()->json(array(
                'success' => false,
                'message' => $validator->getMessageBag()->toArray()
            ));
        }

     
        $data =Ground_detailes::find($request->ground_id);
        $res=new Booking_detailes;
        $res->ground_id=$request->ground_id;
        $res->booking_date=$request->date_of_booking;
        $res->price=$data->priceperday;
        $res->description=$request->desc;
        $res->user_id=Auth::user()->id;
        $res->save();

         return response()->json(array(
                    'success' => true,
                    'message' => "Your Booking Has  successfully Added"
                    
                    ));
     }

	public function get_lawn(Request $request) {

	$id=$request->id;
	$book=Booking_detailes::select('booking_date')->where('ground_id',$id)->where('status',0)->get();
	$date= array();
	foreach ($book as  $value) {
		$date[]=$value->booking_date;
	}

	$res=DB::select(DB::raw("SELECT *  FROM `ground_detailes` where  status=0 And id=$id ")); 
    foreach($res as $list){ 
		$images=Image_master::where('ground_id',$list->id)->where('category','ground_images')->get();
		foreach ($images as $img) {
		$img->url='/get_image/'.$img->category. "/" . $img->year . "/" .$img->month ."/" .$img->image_name."." . $img->ext."/400~400.png";
		unset($img->category,$img->year,$img->image_id,$img->month,$img->image_name, $img->ext,$img->ground_id,$img->created_at,$img->updated_at);
		}
		$list->images=$images; 
	}
	$response['book']=$date;
	$response['response']=$res;
    //die(json_encode($response ));
    return view('website.pages.lawn',['lawns' =>$response]);

}


    public function index(Request $request)
    {

    	// die(json_encode());
	$response['data']['ground'] = array();
	$request_lat =($request->session()->get('latitude'))?$request->session()->get('latitude'):13;
	$request_long =($request->session()->get('longitude'))?$request->session()->get('longitude'):74;
	$earth_radius = 6372.795477598;
	$res=DB::select(DB::raw("SELECT *, ( 3959 * acos ( cos ( radians($request_lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($request_long) ) + sin ( radians($request_lat) ) * sin( radians( latitude ) ) ) ) AS `distance` FROM `ground_detailes`where  status=0  HAVING (distance <=10)")); 
    
     foreach($res as $list){ 
    
 

	//$distance = $miles * 1.609344;
	$distance = round($list->distance) . " KM";
	$images=Image_master::where('ground_id',$list->id)->where('category','ground_images')->get();
	$list->distance = $distance;
	foreach ($images as $img) {
	$img->url='/get_image/'.$img->category. "/" . $img->year . "/" .$img->month ."/" .$img->image_name."." . $img->ext."/330~220.png";
	unset($img->category,$img->year,$img->image_id,$img->month,$img->image_name, $img->ext,$img->ground_id,$img->created_at,$img->updated_at);
	}
	$list->images=$images; 
}




			
  		
        
// die(json_encode($res));

    return view('website.index',['lawns' =>$res]);
    }

    
    public function find_location(Request $request){
	$response['data']['ground'] = array();

	$request_lat = $request->latitude;
	$request_long = $request->longitude;
	Session::put('latitude',$request_lat);
	Session::put('longitude',$request_long);
	Session::put('location',$request->city);
	$earth_radius = 6372.795477598;



	
		$res=DB::select(DB::raw("SELECT *, ( 3959 * acos ( cos ( radians($request->latitude) ) * cos(radians( latitude ) ) * cos( radians( longitude ) - radians($request->longitude) ) + sin(radians($request->latitude) ) * sin( radians( latitude ) ) ) ) AS `distance` FROM `ground_detailes`where  status=0 HAVING (distance <=10)")); 

 	
 	
    
    foreach($res as $list){ 
    
 

	//$distance = $miles * 1.609344;
	$distance = round($list->distance) . " KM";
	$images=Image_master::where('ground_id',$list->id)->where('category','ground_images')->get();
	$list->distance = $distance;
	foreach ($images as $img) {
	$img->url='/get_image/'.$img->category. "/" . $img->year . "/" .$img->month ."/" .$img->image_name."." . $img->ext."/400~350.png";
	unset($img->category,$img->year,$img->image_id,$img->month,$img->image_name, $img->ext,$img->ground_id,$img->created_at,$img->updated_at);
	}
	$list->images=$images; 
}




			
  		
        return response()->json($res);
       
                 
	}

	}