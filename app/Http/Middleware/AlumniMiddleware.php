<?php

namespace App\Http\Middleware;

use Closure;

class AlumniMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request->user()->user_type_id)) {
            if ($request->user()->user_type_id != '201') {
                return redirect('/');
            }
        }
        
        return $next($request);
    }
}
