<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking_detailes extends Model
{
protected $table = 'booking_detailes';
   
    public function requester()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

   
      public function ground()
    {
        return $this->belongsTo('App\Ground_detailes', 'ground_id');
    }   
   
}
