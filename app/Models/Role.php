<?php

namespace App\Models;

use Nanigans\SingleTableInheritance\SingleTableInheritanceTrait;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use SingleTableInheritanceTrait;

    protected $table = "users";

    protected static $singleTableTypeField = 'role_id';

    protected static $singleTableSubclasses = [Admin::class, User::class, Vendor::class];
}
