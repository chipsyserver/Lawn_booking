 @extends('Admin.layouts.master_layout')
@section('content')


 <div class="page">
    <div class="page-header">
      <h1 class="page-title">Edit Lawn Details </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
    
        <li class="breadcrumb-item active">Edit Lawn Details </li>
      </ol>
   
    </div>

    <div class="page-content container-fluid" ng-controller="GoundController">
      <!-- Panel Validation Styles -->
     
      <!-- End Panel Full Example -->

      <!-- Panel Constraints -->
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Add Lawn Details </h3>
        </div>

         <div class="panel-body">
          
          <form id="exampleFullForm" autocomplete="off" name="ground"   >
            <div class="row row-lg">
              <div class="col-xl-6 form-horizontal">
                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Lawn Title
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                    <input type="text" class="form-control" required="required" id="title" name="title" placeholder="Enter The Lawn Name" value="{{$ground->title}}" maxlength="50">
                  </div>
                </div>

                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Latitude
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                  <input type="text" class="form-control" required="required" name="latitude" id="Latitude" placeholder="Enter Latitude" value="{{$ground->latitude}}" onkeypress='validate(event)' maxlength="50" />
                  </div>
                </div>

                 <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Cost Per Day
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                   <input type="text" class="form-control" required="required" name="price_ground"  id="price" placeholder="Enter Price" value="{{$ground->priceperday}}" onkeypress='validate(event)' maxlength="10">
                  </div>
                </div>

                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Address
                    <span class="required">*</span>
                  </label>
                  <div class="col-xl-12 col-md-9">
                    <textarea class="form-control" required="required" rows="3"  name="address" id="address" placeholder="Enter Address Your Ground" value="" maxlength="200">{{$ground->address}}</textarea>
                  </div>
                </div>


               

              </div>

              <div class="col-xl-6 form-horizontal">
             
               <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Vendor Name</label>
                  <div class="col-xl-12 col-md-9">
                       <select class="form-control "  name="user" required="required" >
                  <option  value=" ">Select  Vendor</option>
                   @foreach(App\Models\Vendor::get() as $data)
                   @if($data->id==$ground->user_id)
                  <option value="{{$data->id}}" selected>{{$data->username}}</option>
                  @else
                  <option value="{{$data->id}}">{{$data->username}}</option>
                  @endif
                  @endforeach 
                  </select>
                  </div>
                </div>
                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">longitude
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                  <input type="text" class="form-control" required="required" name="longitude" id="longitude" placeholder="Enter Longitude" value="{{$ground->longitude}}" onkeypress='validate(event)' maxlength="50" />
                  </div>
                </div>

                 <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Best Suited
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                <input type="text" class="form-control"  class="form-control" required="required" name="best_suited" id="best_suited" value="{{$ground->best_suited}}" placeholder="Enter Best Suited" maxlength="25">
                  </div>
                </div>

                  <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Description
                    <span class="required">*</span>
                  </label>
                  <div class="col-xl-12 col-md-9">
                    <textarea class="form-control" required="required" rows="3"  name="description" id="description" placeholder="Enter Description Your Ground" maxlength="200">{{$ground->description}}</textarea>
                  </div>
                </div>
              </div>
                <div class="col-xl-12 col-md-12">
 				<h4 class="example-title">Ground Image</h4>
 				</div>

              @foreach($ground->image as $rs)
              <div class="col-xl-2 col-md-2">
              <div class="example-wrap">
              <div class="example">
               <input type="text" value="{{$rs->image_id}}" name="image_id[]" hidden="true">
              <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="{{$rs->url}}" id name="ground_img[]"  
                  />
              </div>
                 
              </div>
           
            </div>
            @endforeach 

           

          

          

     

              <div class="form-group form-material col-xl-12 text-right padding-top-m">
                <button type="button" class="btn btn-primary" id="{{$ground->id}}" name="update">Update</button>
              </div>
            </div>
          </form>
       
        </div>
      </div>



     
    </div>
  </div>



  <script src="{{asset('backend/jscontroller/create.js')}}"></script>

 

          <script type="text/javascript">
   function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
</script>

  @endsection