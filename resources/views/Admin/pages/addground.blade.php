 @extends('Admin.layouts.master_layout')
@section('content')


 <div class="page">
    <div class="page-header">
      <h1 class="page-title">Add Lawn Details </h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/superadmin/dashboard">Home</a></li>
    
        <li class="breadcrumb-item active">Add Lawn Details </li>
      </ol>
   
    </div>

    <div class="page-content container-fluid" ng-controller="GoundController">
      <!-- Panel Validation Styles -->
     
      <!-- End Panel Full Example -->

      <!-- Panel Constraints -->
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Add Lawn Details </h3>
        </div>

         <div class="panel-body">

          <form id="exampleFullForm" autocomplete="off" name="ground"   >
            <div class="row row-lg">
              <div class="col-xl-6 form-horizontal">
                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Lawn Title
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                    <input type="text" class="form-control" required="required" id="title" name="title" placeholder="Enter The Lawn Name" maxlength="50">
                  </div>
                </div>

                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Latitude
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                  <input type="text" class="form-control" required="required" name="latitude" id="Latitude" placeholder="Enter Latitude" onkeypress='validate(event)' maxlength="50" />
                  </div>
                </div>
                    <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Cost Per Day
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                   <input type="text" class="form-control" required="required" name="price_ground"  id="price" placeholder="Enter Price" onkeypress='validate(event)' maxlength="10">
                  </div>
                </div>
               
                 <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Address
                    <span class="required">*</span>
                  </label>
                  <div class="col-xl-12 col-md-9">
                    <textarea class="form-control" required="required" rows="3"  name="address" id="address" placeholder="Enter Address Your Ground" maxlength="200"></textarea>
                  </div>
                </div>
               

              </div>

              <div class="col-xl-6 form-horizontal">
             
               <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Vendor Name</label>
                  <div class="col-xl-12 col-md-9">
                       <select class="form-control "  name="user" required="required" >
                  <option  value=" ">Select  Vendor</option>
                   @foreach(App\Models\Vendor::get() as $data)
                  <option value="{{$data->id}}">{{$data->user_name}}</option>
                  @endforeach 
                  </select>
                  </div>
                </div>
                <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">longitude
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                  <input type="text" class="form-control" required="required" name="longitude" id="longitude" placeholder="Enter Longitude" onkeypress='validate(event)' maxlength="50" />
                  </div>
                </div>
              
                 <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Best Suited
                    <span class="required">*</span>
                  </label>
                  <div class=" col-xl-12 col-md-9">
                <input type="text" class="form-control"  class="form-control" required="required" name="best_suited" id="best_suited" placeholder="Enter Best Suited" maxlength="25">
                  </div>
                </div>
                 <div class="form-group row form-material">
                  <label class="col-xl-12 col-md-3 form-control-label">Description
                    <span class="required">*</span>
                  </label>
                  <div class="col-xl-12 col-md-9">
                    <textarea class="form-control" required="required" rows="3"  name="description" id="description" placeholder="Enter Description Your Ground" maxlength="200"></textarea>
                  </div>
                </div>
              </div>
                <div class="col-xl-12 col-md-12">
 				<h4 class="example-title">Ground Image</h4>
 				</div>
                  <div class="col-xl-2 col-md-2">
              <!-- Example Default -->
              <div class="example-wrap">
               
                <div class="example">
                   <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" id name="ground_img[]"  required="required" data-parsley-errors-container="#rep_pass_error1"
                  />
                </div>
                 <div id="rep_pass_error1"></div>
              </div>
           
            </div>

            <div class="col-xl-2 col-md-2">
            <div class="example-wrap">
            <div class="example">
            <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" name="ground_img[]"  required="required"  data-parsley-errors-container="#rep_pass_error2"                  />

           </div>
           <div id="rep_pass_error2"></div>
           </div>
           </div>


            <div class="col-xl-2 col-md-2">
            <div class="example-wrap">
            <div class="example">
            <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" name="ground_img[]" required="required"  data-parsley-errors-container="#rep_pass_error3"                  />
           </div>
            <div id="rep_pass_error3"></div>
           </div>
           </div>

           <div class="col-xl-2 col-md-2">
            <div class="example-wrap">
            <div class="example">
            <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" name="ground_img[]" 
                required="required"  data-parsley-errors-container="#rep_pass_error4"                  />
           </div>
            <div id="rep_pass_error4"></div>
           </div>
           </div>

       <div class="col-xl-2 col-md-2">
            <div class="example-wrap">
            <div class="example">
            <input type="file" id="input-file-now-custom-1" data-plugin="dropify" data-default-file="" name="ground_img[]" required="required"  data-parsley-errors-container="#rep_pass_error5"                  />
           </div>
            <div id="rep_pass_error5"></div>
           </div>
           </div>

              <div class="form-group form-material col-xl-12 text-right padding-top-m">
                <button type="submit" class="btn btn-primary" id="submit">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>



     
    </div>
  </div>



  <script src="{{asset('backend/jscontroller/create.js')}}"></script>

 

          <script type="text/javascript">
   function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
</script>

  @endsection