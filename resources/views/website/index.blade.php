 @extends('website.layouts.master_layout')
@section('content')
<div class="main-banner">
    <div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper" data-alias="news-gallery34">
      <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
      <div id="rev_slider_34_1" class="rev_slider" data-version="5.0.7">
        <ul>
          <!-- SLIDE  -->
          <li data-index="rs-129">
            <!-- MAIN IMAGE -->

            <img src="/website/assets/images/banner/banner_1.jpg" alt="" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title tp-resizeme" id="slide-129-layer-1" data-x="['right','right','right','right']" data-hoffset="['100','50','50','30']"
                data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" data-fontsize="['50','50','50','30']"
                data-lineheight="['55','55','55','35']" data-width="['700','700','700','420']" data-height="none" data-whitespace="normal"
                data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
              <div class="banner-text text-right">
                <span>It's an Amazing Services!</span>
                <h2>Decorate your home With Beautyfull Garden</h2>

                <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy
                  text ever.
                </p>
                <a class="btn-text" href="about_us.html"> read more</a>
              </div>
            </div>

          </li>
          <!-- SLIDE  -->
          <li data-index="rs-130" data-title="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="/website/assets/images/banner/banner_3.jpg" alt="" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title tp-resizeme" id="slide-129-layer-2" data-x="['left','left','left','left']" data-hoffset="['100','50','50','30']"
                data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" data-fontsize="['50','50','50','30']"
                data-lineheight="['55','55','55','35']" data-width="['700','700','700','420']" data-height="none" data-whitespace="normal"
                data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
              <div class="banner-text">
                <span>It's an Amazing Services!</span>
                <h2>Extend Your Home With a Beautiful Garden</h2>

                <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy
                  text ever.
                </p>
                <a class="btn-text" href="about_us.html"> read more</a>
              </div>
            </div>

          </li>
          <!-- SLIDE  -->
          <li data-index="rs-131">
            <!-- MAIN IMAGE -->
            <img src="/website/assets/images/banner/banner_4.jpg" alt="" class="rev-slidebg">
            <!-- LAYERS -->

            <!-- LAYER NR. 2 -->
            <div class="tp-caption Newspaper-Title tp-resizeme" id="slide-129-layer-3" data-x="['center','center','center','center']"
                data-hoffset="['0','0','0','0']" data-y="['top','top','top','center']" data-voffset="['165','135','105','0']" data-fontsize="['50','50','50','30']"
                data-lineheight="['55','55','55','35']" data-width="['700','700','700','420']" data-height="none" data-whitespace="normal"
                data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on">
              <div class="banner-text text-center">
                <span>It's an Amazing Services!</span>
                <h2>Helping you improve Your garden appearance</h2>

                <p>
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy
                  text ever.
                </p>
                <a class="btn-text" href="about_us.html"> read more</a>
              </div>
            </div>
          </li>
          <!-- SLIDE  -->
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
      </div>
    </div>
  </div>
  <!--  Main Banner End Here-->

  <!-- About_Compney -->

  <div id="services-section" class="padding ptb-xs-40">
      <div class="container">
        <div class="row text-center">
          <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 mb-30">
            <div class="heading-box">
             <h2><span>Lawn</span> Details</h2>
            </div>
          <div class="widget-search pt-15">
            <!--   <input class="form-full input-lg" type="text" value="" placeholder="Search Here" name="search" id="wid-search"> -->
              <!--   <input type='text' id="autocomplete" name="loc" required="true" placeholder='Enter a location'  class="login__input form-full input-lg" autocomplete="on" runat="server">
             -->
            </div>
        

          </div>
        </div>
        <div class="row" id="lawn_append">
          @foreach ($lawns as $data)
          <div class="col-lg-4 col-md-6 full-wid">
            <div class="project-item">
                <div class="about-block clearfix">
                  <figure>
                    <a href="/lawn/{{$data->id}}"><img class="img-responsive" src="{{$data->images[0]->url}}" alt="Photo"></a>

                  </figure>
                  <div class="text-box mt-25">
                    <div class="box-title mb-15">

                      <h3> <a href="/lawn/{{$data->id}}">{{$data->title}}</a> </h3>
                    </div>
                    <div class="text-content">
                      <p>
                     {{$data->address}}
                      </p>
                      <a href="/lawn/{{$data->id}}" class="btn-text  mt-15">Read More</a>
                      <!-- <a href="#ViewModal" data-toggle="modal" class="btn-text mt-15" style="float: right;">Quick View</a> -->
                    </div>
                  </div>
                </div>
              </div>

          </div>
          @endforeach
          @if(!count($lawns))
           <div class="col-lg-12 col-md-12 full-wid" style="    text-align: center;">
          <img class="img-responsive" src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_6.jpg" alt="Photo">
          </div>

          @endif


</div>
</div>
</div>

 
        

 
  <!-- About_Compney_End-->

 
  <!--Testimonial Section Start-->
  <section class="testimonial_wrapper__block padding ptb-xs-40">
    <div class="container">
      <div class="row text-center mb-30">
        <div class="col-sm-12">
          <div class="sec_hedding">
            <h2>
              <span>Our</span> Testimonial</h2>
            <span class="b-line"></span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="testimonial_carosule-wrap owl-carousel">
            <div class="single_carousel pt-40">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem [..]
              </p>
              <div class="author_img__block">
                <div class="author_tablecell__block">
                  <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/author.jpg" class="rounded-circle" alt="">
                  <p>
                    John Doe
                    <span>CEO</span>
                  </p>
                </div>
              </div>
            </div>

            <div class="single_carousel pt-40">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem [..]
              </p>
              <div class="author_img__block">
                <div class="author_tablecell__block">
                  <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/author.jpg" class="rounded-circle" alt="">
                  <p>
                    John Doe
                    <span>CEO</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="single_carousel pt-40">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem [..]
              </p>
              <div class="author_img__block">
                <div class="author_tablecell__block">
                  <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/author.jpg" class="rounded-circle" alt="">
                  <p>
                    John Doe
                    <span>CEO</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="single_carousel pt-40">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore Lorem [..]
              </p>
              <div class="author_img__block">
                <div class="author_tablecell__block">
                  <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/author.jpg" class="rounded-circle" alt="">
                  <p>
                    John Doe
                    <span>CEO</span>
                  </p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Testimonial Section End-->

  <!-- Service tab Start-->
  <section class="padding ptb-xs-40 gray-bg">
    <div class="container">
      <div class="row text-center mb-30">
        <div class="col-sm-12">
          <div class="sec_hedding">
            <h2>
              <span>Our</span> Service</h2>
            <span class="b-line"></span>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-4">

          <a href="#!" class="top-service tab_funct d-flex align-items-center active" data-tab="service-tab">
            <div class="fr-text">
              <i class="hi-icon flaticon-black"></i>
              <span>Rubbish Removal</span>

            </div>
          </a>

          <a href="#!" class="top-service tab_funct d-flex align-items-center" data-tab="service-tab-two">
            <div class="fr-text">
              <i class="hi-icon flaticon-people"></i>
              <span>Lawn Moving</span>
            </div>
          </a>

          <a href="#!" class="top-service tab_funct d-flex align-items-center" data-tab="service-tab-three">
            <div class="fr-text">
              <i class="hi-icon flaticon-nature-1"></i>
              <span>Garden Care</span>
            </div>
          </a>

          <a href="#!" class="top-service tab_funct d-flex align-items-center" data-tab="service-tab-fouth">
            <div class="fr-text">
              <i class="hi-icon flaticon-nature"></i>
              <span>Landscape Design</span>
            </div>
          </a>

          <a href="#!" class="top-service tab_funct d-flex align-items-center" data-tab="service-tab-five">
            <div class="fr-text">
              <i class="hi-icon flaticon-people-2"></i>
              <span>Excellent Services</span>
            </div>
          </a>

          <a href="#!" class="top-service tab_funct d-flex align-items-center" data-tab="service-tab-six">
            <div class="fr-text">
              <i class="hi-icon flaticon-fence"></i>
              <span>Insect Control</span>
            </div>
          </a>

        </div>

        <div class="col-lg-8 mt-xs-30">
          <div class="tab-content color-white">
            <div class="service-test plr-20 service-tab mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_1.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_5.jpg" alt="" />
                    </div>
                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Rubbish Removal</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>



                  </div>
                </div>

              </div>
            </div>

            <div class="service-test plr-20 service-tab-two mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_2.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_4.jpg" alt="" />
                    </div>

                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Lawn Moving</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

            <div class="service-test plr-20 service-tab-three mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_1.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_6.jpg" alt="" />
                    </div>

                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Garden Care</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

            <div class="service-test plr-20 service-tab-fouth mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_3.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_5.jpg" alt="" />
                    </div>

                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Landscape Design</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

            <div class="service-test plr-20 service-tab-five mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_2.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_5.jpg" alt="" />
                    </div>

                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Excellent Services</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

            <div class="service-test plr-20 service-tab-six mt-sm-30">
              <div class="row">

                <div class="col-lg-12 bg_same">
                  <div class="row">
                    <div class="col-md-6 mb-xs-30">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_6.jpg" alt="" />
                    </div>
                    <div class="col-md-6">
                      <img src="http://theembazaar.com/demo/themesfolios/gardenex/assets/images/service/img_1.jpg" alt="" />
                    </div>

                    <div class="col-md-4 push-md-8 mt-40 mt-xs-20">
                      <div class="impotent_point">
                        <ul>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Magna aliqua enim
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Buka amin
                          </li>
                          <li>
                            <i class="ion-chevron-right"></i> Labore et dolore
                          </li>


                        </ul>
                      </div>
                    </div>
                    <div class="col-md-8  pull-md-4">
                      <h4>Insect Control</h4>
                      <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...
                      </p>
                      <a href="#!" class="btn-text">Read More</a>
                    </div>

                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </section>
  <!--Service tab End-->









    <!--Blog Section End-->
  @endsection