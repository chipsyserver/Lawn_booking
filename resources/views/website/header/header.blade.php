<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gardenex  </title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,700" rel="stylesheet">
  <link href="/website/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/website/assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
  <link href="/website/assets/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="/website/assets/css/ionicons.css" rel="stylesheet" type="text/css">
  <link href="/website/assets/css/flaticon.css" rel="stylesheet" type="text/css">
  <link href="/website/assets/css/simple-line-icons.css" rel="stylesheet" type="text/css">

  <link href="/website/assets/css/jquery-ui.min.css" type="text/css" rel="stylesheet">
  <link href="/website/assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
  <!--Main Slider-->
  <link href="/website/assets/css/settings.css" type="text/css" rel="stylesheet" media="screen">
  <link href="/website/assets/css/layers.css" type="text/css" rel="stylesheet" media="screen">

  <link href="/website/assets/css/style.css" type="text/css" rel="stylesheet">
  <link href="/website/assets/css/index.css" type="text/css" rel="stylesheet">
  <link href="/website/assets/css/header.css" type="text/css" rel="stylesheet">
  <link href="/website/assets/css/footer.css" type="text/css" rel="stylesheet">
  <link href="/website/assets/css/prettyPhoto.css" rel="stylesheet" />
  <link href="/website/assets/css/theme-color/default.css" rel="stylesheet" type="text/css" id="theme-color" />
<link rel="stylesheet" type="text/css" href="{{asset('/backend/assets/parsley/parsley.css')}}">
 <script src="/website/jquery/jquery-min.js"></script>


    <script src="/website/js/parsley.min.js"></script>
    <script src="/website/js/fileuploade.js"></script>

</head>

   