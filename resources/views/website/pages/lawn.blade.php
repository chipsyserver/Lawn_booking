 @extends('website.layouts.master_layout')
@section('content')
<link rel="stylesheet" type="text/css" href="/backend/assets/bootstrap-datepicker-master/dist/css/bootstrap-datepicker.min.css">

 @foreach ($lawns['response'] as $data)
<section class="inner-intro bg-img light-color overlay-before parallax-background" >
    <div class="container">
      <div class="row title">
        <div class="title_row">
          <h1 data-title="Login"><span>{{$data->title}}</span></h1>
          <div class="page-breadcrumb">
              <a>Home</a>/ <span>{{$data->title}}</span>
            </div>
          
        </div>
        
      </div>
    </div>
  </section>

 <!-- Intro Section End-->

  <!-- Login Section -->
  
<section class="pt-90 pb-50 ptb-xs-60">
        <div class="container shop">
          <div class="row">
            <div class="col-sm-12 col-md-12 product-page">
              <div class="row">
                <div class="col-md-4 col-sm-6">
                  <div class="single-product">
                    <img id="zoom-product" src="{{$data->images[0]->url}}"  data-zoom-image="{{$data->images[0]->url}}" alt="" />
                    <div id="zoom-product-thumb" class="zoom-product-thumb">

                      <div class="owl-carousel nf-carousel-theme owl-carousel_product navigation-shop dark-switch lr-pad-20" data-pagination="false" data-autoplay="false" data-navigation="true" data-items="3" data-tablet="4" data-mobile="3" data-prev="fa fa-chevron-left" data-next="fa fa-chevron-right">
                      <?php $i=1;?>
                      @foreach ($data->images as $img)
                        <a href="#" data-image="{{$img->url}}" data-zoom-image="{{$img->url}}" class="img__block"> <img id="img_0{{$i++}}" src="{{$img->url}}" alt="" /> </a>
                        @endforeach
                       <!--  <a href="#" data-image="/get_image/ground_images/2018/08/5b7a8b54f3c98.jpg/400~350.png" data-zoom-image="/get_image/ground_images/2018/08/5b7a8b54f3c98.jpg/400~350.png" class="img__block"> <img id="img_02" src="/get_image/ground_images/2018/08/5b7a8b54f3c98.jpg/400~350.png" alt=""/> </a>

                        <a href="#" data-image="/get_image/ground_images/2018/08/5b7a8b551fbba.jpg/400~350.png" data-zoom-image="/get_image/ground_images/2018/08/5b7a8b551fbba.jpg/400~350.png" class="img__block"> <img id="img_03" src="/get_image/ground_images/2018/08/5b7a8b551fbba.jpg/400~350.png" alt=""/> </a>

                        <a href="#" data-image="/get_image/ground_images/2018/08/5b7a8b555bcc5.jpg/400~350.png" data-zoom-image="/get_image/ground_images/2018/08/5b7a8b555bcc5.jpg/400~350.png" class="img__block"> <img id="img_04" src="/get_image/ground_images/2018/08/5b7a8b555bcc5.jpg/400~350.png" alt=""/> </a>
                        
                        <a href="#" data-image="/get_image/ground_images/2018/08/5b7a8b5580ef1.jpg/400~350.png" data-zoom-image="/get_image/ground_images/2018/08/5b7a8b5580ef1.jpg/400~350.png" class="img__block"> <img id="img_0{{$i}}" src="/get_image/ground_images/2018/08/5b7a8b5580ef1.jpg/400~350.png" alt=""/> </a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <!-- .product -->
                <div class="col-md-8 col-sm-6">
                 <div class="price-details">
                   <!-- <span class="actual-price">Rs.98.00</span> -->
                    <span class="price">Rs. {{$data->priceperday}}</span>
                    <input type="hidden" name="price_amt" value="{{ $data->priceperday }}">
                  </div>
                  <div class="description">
                    <p>
                     {{$data->description}}
                    </p>
                   
                  </div>


                    <a href="#createLawn" data-toggle="modal" name="book_now" class="btn-text white-btn">Book Now</a>
                    <br>
                <!--   <ul class="arrow-style">
                    <li>
                      <i class="ion-android-done-all text-color"> </i> Best Quality
                    </li>
                    <li>
                      <i class="ion-android-done-all text-color"> </i> High cleaner
                    </li>
                    <li>
                      <i class="ion-android-done-all text-color"> </i> Machine Wash Warm
                    </li>
                  </ul> --> 
                
                  <div class="product-meta-details">
                 <div class="description">
                             <p><span class="product-code">Best Suited For:  <b> {{$data->best_suited}} </b></span></p>
                      <h4>Address</h4>
                        <p>
                          {{$data->address}}
                        </p>
                   <!--  <span class="product-code pull-right">Category: Finance Book, Wood Cleaner</span>
                    <div class="product-tag">
                      <strong>Tags:</strong>
                      <span class="text-color">Finance Book, Wood Cleaner</span>
                    </div> -->
                  </div>
                  
                    
                   <!--  <span class="product-code pull-right">Category: Finance Book, Wood Cleaner</span>
                    <div class="product-tag">
                      <strong>Tags:</strong>
                      <span class="text-color">Finance Book, Wood Cleaner</span>
                    </div> -->
                  </div>
                </div>
              </div>
              
           
            </div>
         </div>

          </div>
       
      </section>
 @endforeach



 <div class="modal fade " id="createLawn" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Book</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
              <form class="form-horizontal ng-pristine ng-valid" action="/submit-booking"  method="POST" role="form">
               <fieldset>
                <div class="form-group">
                        <div class="ui-input-group">
                      <input type="text" name="ground_id" value="{{Request::segment(2)}}" hidden="true">
                      <input type="text" id="datepicker" class="form-control date" value="" name="date_of_booking" title="Date of Booking" placeholder="Date of Booking"  > 
                          <span class="input-bar"></span>
                        <label>Date</label>
                        </div>
                      </div>



                     <!--   <div class="form-group">
                        <div class="ui-input-group">
                            <input required type="email" name="email" class="form-control" placeholder="">
                          <span class="input-bar"></span>
                        <label>Email</label>
                        </div>
                      </div> -->
                       <div class="form-group">
                              <label>Description</label>
                        <div class="">

                         <textarea name="desc" id="desc" required class="form-control" rows="3"></textarea>
                          <span class="input-bar"></span>
                     
                        </div>
                      </div>
         
                <div class="form-group formField" style="text-align: center;">
                  <input type="button" name="booking_lawn" class="btn btn-primary btn-block bg-color-3 border-color-3" value="Book" style="width: 200px">
                </div>
                <div class="form-group formField">
                  <p class="help-block signup-error"> </p>
                </div>
               
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>

<script type="text/javascript">
  // $('#createLawn').modal(keyboard: false);

</script>
<style type="text/css">
  .modal {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 9999999;
    display: none;
  }
  #loc{
    display: none;
  }
</style>
<script src="{{asset('backend/assets/bootstrap-datepicker-master/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript">
var date='<?php echo json_encode($lawns['book']);?>';
    $('#datepicker1').datepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
   
    minView: 2,
     format: 'yyyy-mm-dd',
     startDate: new Date(),
    forceParse: 0
    });
  


$('#datepicker').datepicker({
   language:  'fr',
        weekStart: 1,
        todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
     minView: 2,
   format: 'yyyy-mm-dd',
     startDate: new Date(),
  datesDisabled: date
}).on('changeMonth', function(e){
  var month = e.date.getMonth();
 //  $('#datepicker').datepicker('setDatesDisabled', datesDisabled);
});


</script>

 
 <?php
  if(isset($_GET['date_of_booking']))
    { ?>
  
  <script type="text/javascript">
   $(document).ready(function(){
       $('[name=date_of_booking]').val('<?php echo $_GET['date_of_booking'];  ?>');
       $('[name=desc]').val('<?php echo $_GET['desc'];  ?>');
       setTimeout(function(){
         $('#createLawn').modal('show');
       }, 3000);
   });
 </script>
  
 <?php } ?>
   @endsection


 