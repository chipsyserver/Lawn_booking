<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">

  <title>Login | Vendor </title>

  <link rel="apple-touch-icon" href="/backend/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="/backend/assets/images/favicon.ico">

  <!-- Stylesheets -->
  <link rel="stylesheet" href="/backend/css/bootstrap.min.css">
  <link rel="stylesheet" href="/backend/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="/backend/assets/css/site.min.css">

  <!-- Skin tools (demo site only) -->

  <!-- Plugins -->

  <!-- Page -->
  <link rel="stylesheet" href="/backend/assets/examples/css/pages/login-v3.min.css">

  <!-- Fonts -->
  <link rel="stylesheet" href="/backend/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="/backend/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic">


 <script src="/backend/assets/js/jquery.min.js"></script>
  
  
</head>
<body class="animsition site-navbar-small page-login-v3 layout-full" ng-app="TapmiAdmin" >
          
        <div class="login-box" ng-controller="LoginController">   


  <!-- Page -->
  <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
    <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
      <div class="panel">
        <div class="panel-body">
          <div class="brand">
            <img class="brand-img" src="../backend/assets/images/logo-colored.png" alt="...">
            <h2 class="brand-text font-size-18">Remark</h2>
          </div>
           <form id="sign_in" action="{{url('vendor/auth-login')}}" method="post" name="super_login" >
            {{csrf_field()}}  
                        <input type="hidden" name="_token" value="1Vg9ztkT4d7d7srf9BeKDjxUy3Baba0qU7f0QMn7">  
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="email" class="form-control" placeholder="Email" name="email" required="true" autofocus />
              <!-- <label class="floating-label">User Name</label> -->
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control" placeholder="Password" name="password" required="true" />
            <!--   <label class="floating-label">Password</label> -->
            </div>

                        <div class="col-xs-8 p-t-5">          
                                <div name="message_area">
                                    <label></label>
                                </div>  
                            </div>
           <!--  <div class="form-group clearfix">
              <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                <input type="checkbox" id="inputCheckbox" name="remember">
                <label for="inputCheckbox">Remember me</label>
              </div>

            </div> -->
            <button type="submit" class="btn btn-primary btn-block btn-lg mt-40" name="submit-login">Sign in</button>
          </form>
          
        </div>
      </div>

      
    </div>
  </div>
  </div>
  <!-- End Page -->


  <!-- Core  -->
 <script src="/backend/vendor/jquery/jquery.min.js"></script>
        <script src="{{asset('/backend/assets/js/jquery-loader.js')}}" type="application/javascript"></script>

      <script src="{{asset('/backend/assets/js/file_upload.js')}}"></script>

 <script src="{{asset('/backend/assets/parsley/parsley.js')}}"></script>
  <script src="/backend/vendor/popper-js/umd/popper.min.js"></script>
  <script src="/backend/vendor/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript">
   $.loader = {
                start: function(callback) {
                    $('#preloader').css('display', 'block')
                },
                stop: function(callback) {
                    $('#preloader').css('display', 'none')
                },
            }
            $.loader.start();


            window.onload = function() {
                $.loader.stop();
            }
   $(function () {
                $(document).on('keyup', '.form-control', function () {
                    if (!$(this).val()) {
                        $('[name="res-msg"]').addClass('hidden').find('span').html('');                        
                    }
                });
                
                // Setting focus
                $('input[name="email"]').focus();
                // Setting width of the alert box
                var formWidth = $('.bootstrap-admin-login-form').innerWidth();
                var alertPadding = parseInt($('.alert').css('padding'));
                $('.alert').width(formWidth - 2 * alertPadding);

                $('[name="log_submit"]').click(function(e) {
                    e.preventDefault();
                    var proceed = true;
                    proceed = $('form[name="super_login"]').parsley().validate();

                    if (proceed) {
                      $('form[name="super_login"]').trigger('submit');
                    }
                });

                $(document).on('keypress', '.form-control',function (e) {
                    if (e.which == 13) {
                        var proceed = true;
                        proceed = $('form[name="super_login"]').parsley().validate();

                        if (proceed) {
                          $('form[name="super_login"]').trigger('submit');
                        }
                    }
                });

                $('form[name="super_login"]').submit(function(e) {
                 
                    var $this = $(this);
                    e.preventDefault();
                    $this.ajaxSubmit({
                        beforeSubmit: function() {
                            $.loader.start();                                  
                        },
                        uploadProgress: function(event, position, total, percentComplete) {
                            $.loader.start();
                        },
                        success: function(response) {
                            //$('[name="res-msg"]').removeClass('hidden').find('span').html(response.message);
                            if (response.success) {
                                $('[name="res-msg"]').removeClass('alert-danger').addClass('alert-info');
                                setTimeout(function() {location.href='/vendor/dashboard';},2000);         
                            } else {
                                $.loader.stop();
                               var display = "";
                                if ((typeof response.message) == 'object') {
                                    $.each(response.message, function(key, value) {
                                        display = value[0];
                                    });
                                } else {
                                    display = response.message;
                                }
                               $('[name=message_area]').html(display).css('color', 'red');
                            }
                        },
                        error: function(jqXHR, status, error) {
                            $('[name="res-msg"]').removeClass('hidden').addClass('alert-danger').find('span').html(error);
                        },
                    });
                });
            });
        
</script>
  
</body>

</html>