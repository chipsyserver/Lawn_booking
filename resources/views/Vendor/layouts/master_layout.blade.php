<html ng-app="lawn">
@include('Vendor.header.header')


<style type="text/css">
    #divLoading
{
display : none;
}
#divLoading.show
{
display : block;
position : fixed;
z-index: 100;
background-image : url('http://loadinggif.com/images/image-selection/3.gif');
/*background-color:#666;*/
opacity : 0.4;
background-repeat : no-repeat;
background-position : center;
left : 0;
bottom : 0;
right : 0;
top : 0;
}
#loadinggif.show
{
left : 50%;
top : 50%;
position : absolute;
z-index : 101;
width : 32px;
height : 32px;
margin-left : -16px;
margin-top : -16px;
}
div.content {
width : 1000px;
height : 1000px;
}
  </style>

<body class="animsition site-navbar-small dashboard">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse"
    role="navigation">

    <div class="navbar-header">
      <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
        data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
        data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <a class="navbar-brand navbar-brand-center" href="/vendor/dashboard">
        <img class="navbar-brand-logo navbar-brand-logo-normal" src="/backend/assets/images/logo.png"
          title="Remark">
        <img class="navbar-brand-logo navbar-brand-logo-special" src="/backend/assets/images/logo-colored.png"
          title="Remark">
        <span class="navbar-brand-text hidden-xs-down"> Remark</span>
      </a>
    
    </div>

    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        {{--<ul class="nav navbar-toolbar">
          <li class="nav-item hidden-float" id="toggleMenubar">
            <a class="nav-link" data-toggle="menubar" href="#" role="button">
                <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
              </a>
          </li>
          <li class="nav-item hidden-sm-down" id="toggleFullscreen">
            <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
              <span class="sr-only">Toggle fullscreen</span>
            </a>
          </li>
          <li class="nav-item hidden-float">
            <a class="nav-link icon md-search" data-toggle="collapse" href="#" data-target="#site-navbar-search"
              role="button">
              <span class="sr-only">Toggle Search</span>
            </a>
          </li>
          <li class="nav-item dropdown dropdown-fw dropdown-mega">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false" data-animation="fade"
              role="button">Mega <i class="icon md-chevron-down" aria-hidden="true"></i></a>
            <div class="dropdown-menu" role="menu">
              <div class="mega-content">
                <div class="row">
                  <div class="col-md-4">
                    <h5>UI Kit</h5>
                    <ul class="blocks-2">
                      <li class="mega-menu m-0">
                        <ul class="list-icons">
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="advanced/animation.html">Animation</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/buttons.html">Buttons</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/colors.html">Colors</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/dropdowns.html">Dropdowns</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/icons.html">Icons</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="advanced/lightbox.html">Lightbox</a>
                          </li>
                        </ul>
                      </li>
                      <li class="mega-menu m-0">
                        <ul class="list-icons">
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/modals.html">Modals</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/panel-structure.html">Panels</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="structure/overlay.html">Overlay</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/tooltip-popover.html ">Tooltips</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="advanced/scrollable.html">Scrollable</a>
                          </li>
                          <li><i class="md-chevron-right" aria-hidden="true"></i>
                            <a
                              href="uikit/typography.html">Typography</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
            
                </div>
              </div>
            </div>
          </li>
        </ul>--}}
        <!-- End Navbar Toolbar -->

        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
       
          <li class="nav-item dropdown">
            <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
              data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="/backend/portraits/5.jpg" alt="...">
               <!--  <i></i> -->
              </span>
            </a>
            <div class="dropdown-menu" role="menu">
             
<!--               <a class="dropdown-item" href="/vendor/settings" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
              <div class="dropdown-divider"></div> -->
              <a class="dropdown-item" href="/vendor/signout" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
            </div>
          </li>
         
          <!-- <li class="nav-item" id="toggleChat">
            <a class="nav-link" data-toggle="site-sidebar" href="javascript:void(0)" title="Chat"
              data-url="site-sidebar.tpl">
                <i class="icon md-comment" aria-hidden="true"></i>
              </a>
          </li> -->
        </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->

      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Search...">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar site-menubar-light">
    <div class="site-menubar-body">
      <div>
        <div>

          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-category">General</li>

            <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="/vendor/dashboard" >
                                <i class="site-menu-icon md-widgets" aria-hidden="true"></i>
                                <span class="site-menu-title">Dashbord</span>
                                   <!--  <span class="site-menu-arrow"></span> -->
                            </a>
                            </li>
          <!-- <li class="dropdown site-menu-item has-sub">
              <a data-toggle="dropdown" href="/vendor/lawn" data-dropdown-toggle="false">
                                <i class="site-menu-icon  md-book" aria-hidden="true"></i>
                                <span class="site-menu-title">Lawn book </span>
                            
                            </a>
             </li> -->
               
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </li>
          
          </ul>

        </div>
      </div>
    </div>

  </div>

    <div class="page-wrapper">
      @yield('content')
      
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->

  @include('Vendor.footer.footer')

  <style type="text/css">
    .dashboard .card, .dashboard .panel {
    height: -webkit-calc(100% - 30px);
    height: auto;
    margin-bottom: 30px;
}
.pagination-first{
    color: #6c757d;
    pointer-events: none;
    cursor: auto;
    background-color: #fff;
    border-color: #dee2e6;
}

.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px;
}
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #23527c;
  background-color: #eee;
  border-color: #ddd;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd;
}
.pagination-lg > li > a,
.pagination-lg > li > span {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.3333333;
}
.pagination-lg > li:first-child > a,
.pagination-lg > li:first-child > span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px;
}
.pagination-lg > li:last-child > a,
.pagination-lg > li:last-child > span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px;
}
.pagination-sm > li > a,
.pagination-sm > li > span {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
}
.pagination-sm > li:first-child > a,
.pagination-sm > li:first-child > span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px;
}
.pagination-sm > li:last-child > a,
.pagination-sm > li:last-child > span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
}
.pager {
  padding-left: 0;
  margin: 20px 0;
  text-align: center;
  list-style: none;
}
.pager li {
  display: inline;
}
.pager li > a,
.pager li > span {
  display: inline-block;
  padding: 5px 14px;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 15px;
}
.pager li > a:hover,
.pager li > a:focus {
  text-decoration: none;
  background-color: #eee;
}
.pager .next > a,
.pager .next > span {
  float: right;
}
.pager .previous > a,
.pager .previous > span {
  float: left;
}
.pager .disabled > a,
.pager .disabled > a:hover,
.pager .disabled > a:focus,
.pager .disabled > span {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
}
  </style>
</body>
</html>
