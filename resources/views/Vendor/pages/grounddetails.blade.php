 @extends('Vendor.layouts.master_layout')
@section('content')
<meta charset='utf-8' />
<link href='/calender/fullcalendar.min.css' rel='stylesheet' />
<link href='/calender/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='/calender/lib/moment.min.js'></script>
<script src='/calender/lib/jquery.min.js'></script>
<script src='/calender/fullcalendar.min.js'></script>
<script>


  

 

</script>
<style>

  

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
.fc-past{
    background-color: #eeeeee;
}
</style>
 <?php
  $segment=0;

   $segment =  Request::segment(4);  

 ?>
 <input type="text" id="ground_id" name="ground_id" value="{{$segment}}" hidden="true">
 <div class="page-header">
      <h1 class="page-title">Booking ground</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/vendor/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Booking ground</li>
      </ol>
    
    </div>



    <div class="page">
   
    <div class="page-main">
      <div class="calendar-container">

  <div id='calendar'></div>
 <div class="modal fade" id="addNewEvent" aria-hidden="true" aria-labelledby="addNewEvent"
          role="dialog" tabindex="-1">
          <div class="modal-dialog modal-simple">
            <form class="modal-content form-horizontal" name="booking" action="#" method="post" role="form">
              <div class="modal-header">
                <button type="button" class="close" aria-hidden="true" data-dismiss="modal">×</button>
                <h4 class="modal-title">Booking Details</h4>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                  <label class="col-md-2 form-control-label" for="ename">Description:</label>
                  <div class="col-md-10">
                  <textarea class="form-control" id="ename" name="ename"></textarea>
                 
                  </div>
                </div>
                
                 <input type="text" class="form-control" id="date" name="date" hidden="true">
             <!--    <div class="form-group row">
                  <label class="col-md-2 form-control-label" for="ends">Ends:</label>
                  <div class="col-md-10">
                    <div class="input-group">
                      <input type="text" class="form-control" id="ends" data-container="#addNewEvent"
                        data-plugin="datepicker">
                      <span class="input-group-addon">
                        <i class="icon md-calendar" aria-hidden="true"></i>
                      </span>
                    </div>
                  </div>
                </div> -->
                
           
              </div>
              <div class="modal-footer">
                <div class="form-actions">
                  <button class="btn btn-primary" id="submit" type="button">Add this Booking</button>

                  <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
              </div>
            </form>
          </div>
        </div>

    <div class="modal fade" id="addNewEvent1" aria-hidden="true" aria-labelledby="addNewEvent1"
          role="dialog" tabindex="-1">
          <div class="modal-dialog modal-simple">
            <form class="modal-content form-horizontal" name="bookingcancel" action="#" method="post" role="form">
              <div class="modal-header">

        <!--         <button type="button" class="close btn btn-danger" aria-hidden="true" data-dismiss="modal">×</button> -->
              
                <div class="col-lg-6" style="padding-left: 0px"><h4 class="modal-title">Booking Details      </h4></div>
                <div class="col-lg-6" style="padding-right: 0px;"><button type="button" class="btn btn-danger " id="submitcancel" type="button"  style="float: right;">Cancel this Booking</button></div>
                
              </div>
              <div class="modal-body" >
                
                <div class="example-grid" id="addbook">
                          
              </div>
              <div class="modal-footer">

                <div class="form-actions">
                 
                  <a class="btn btn-sm btn-white btn-pure" data-dismiss="modal" href="javascript:void(0)">Cancel</a>
                </div>
              </div>
            </form>
          </div>
        </div>

        </div>
        </div>
        </div>
        <script src="{{asset('backend/jscontroller/book_vendor.js')}}"></script>
@endsection