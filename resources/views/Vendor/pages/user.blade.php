 @extends('Admin.layouts.master_layout')
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('assets/parsley/parsley.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/jquery-confirm/jquery-confirm.css')}}">

 <div class="page">
    <div class="page-header">
      <h1 class="page-title">Vendor Details</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/vendor/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Vendor Creations</li>
      </ol>
    
    </div>

    <div class="page-content container-fluid" ng-controller="userController" ng-cloak>
      <div class="row">
      
        <div class="col-md-5">
          <!-- Panel Static Labels -->
          	<div class="panel">
            <div class="panel-heading">
              <h3 class="panel-title">Add Vendor</h3>
            </div>


            <div class="panel-body container-fluid">

          
              <form class="form-horizontal"  autocomplete="off" ng-enter="profile()" name='user_profile'>
              
               <div class="form-group row form-material row">
                      <label class="col-md-4 form-control-label">Email</label>
                       <div class="col-md-8">
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email" maxlength="25" required="true" />
                  </div>
                </div>
              <div class="form-group row form-material row">
                      <label class="col-md-4 form-control-label">User Name</label>
                   <div class="col-md-8">
                  <input type="text" class="form-control" id="username" name="username" placeholder="Name" maxlength="25" maxlength="20" required="true" />
                  </div>
                </div>
                      
              <div class="form-group row form-material row">

                      <label class="col-md-4 form-control-label">Mobile Number:</label>
                       <div class="col-md-8">
                 <input type="text"  required="required" name="phone" id="phone" class="form-control" placeholder="Mobile Number"     data-parsley-validation-threshold="1" data-parsley-trigger="keyup"   data-parsley-type="digits" maxlength="20">
                 </div>
                </div>

             
            
     			<div class="form-group row form-material row">
                      <label class="col-md-4 form-control-label">Password</label>

                  <div class="col-md-8">
                    <input type="password"  required="required" name="new_password" id="new_password" class="form-control" placeholder="Password" minlength="5" maxlength="30" data-parsley-errors-container="#new_pass_error" >
    
                           
                            
                            <div id="new_pass_error"></div>
          </div>
                </div> 
                         
                         
                    <div class="form-group row form-material row">
                      <label class="col-md-4 form-control-label">Confirm Password</label>

                 	 <div class="col-md-8">
                    <input type="password"  value="" required="required" data-parsley-equalto="#new_password" data-parsley-equalto-message="Confirm Password should match the New Password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Repeat Password" data-parsley-errors-container="#rep_pass_error" maxlength="30" minlength="5">

                            <div id="rep_pass_error"></div>
                  </div>
                </div>      
            
                 <div class="form-group row form-material row">
                      <div class="col-md-9 offset-md-3">
              <!--    <input   type="reset"   class="btn btn-default waves-effect" value="Reset"> -->
             <button type="button" ng-if="showBtns" ng-hide="showSave " ng-click="profile()"  class="btn btn-primary waves-effect pull-right">Submit</button>
                               
                                <button type="button" ng-if="showBtns" ng-show="showSave"   ng-click="update($event)" id=""   name="update" class="btn btn-primary updatewarden waves-effect pull-right">Update</button>
                                 <button type="reset" class="btn btn-warning waves-effect waves-classic">Reset</button>

              </div>
              </div>
              </form>
            </div>
          </div>
          <!-- End Panel Static Labels -->
          </div>
          
          <div class="col-md-7" id="editdisable">
 			 <div class="panel">
      	  <div class="panel-body container-fluid">
        
       <div class="example-wrap">
                <h4 class="example-title">Total Vendor ( <%totalUsers%> )</h4>

         
            <!-- /.box-header -->
             <div class="example table-responsive">
                  <table class="table">
                    <thead>
                    <tr>
                    <th>Sl_no</th>
                    <th>Email_ID</th>
                    <th>User_Name</th>
                    <th>Create_Date</th>
                    <th colspan="2">Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                                      <tr ng-repeat="user in users" id="<%user.id%>">
                                  <th scope="row"><%(usersPerPage * (currentPage-1)) + $index+1  %></th>
                                    <td><% user.email %></td>
                                    
                                     <td><% user.username %> </td>
                                    
                                    <td><% user.registered_on %> </td>
                                    <td>
                                    <button id="<%user.id%>" type="button" class="btn btn-icon btn-info btn-sm waves-effect waves-classic" ng-click="edit($event)"><i class="icon md-edit" aria-hidden="true"></i></button>
           
                                    </td>
                                    <td>

                                     <button id="<%user.id%>" type="button" class="btn btn-icon btn-danger btn-sm waves-effect waves-classic" data-ng-click="delete($event)"><i class="icon md-delete" aria-hidden="true"></i></button>
                               

                                        </td>
                                </tr>
                                   
               
              </tbody>
              </table>

               <div class="box-footer clearfix">
           				  <div class=" text-left">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination " boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>

            </div>
          </div>
            <!-- /.box-body -->

           
          </div>



          </div>
        </div>
        </div>
       </div>
       </div>
       </div>

  



 <script src="{{asset('backend/jscontroller/user.js')}}"></script>
<script type="text/javascript">
   function validate(evt) {
   var theEvent = evt || window.event;
   var key = theEvent.keyCode || theEvent.which;
   key = String.fromCharCode( key );
   var regex = /[0-9]|\./;
   if( !regex.test(key) ) {
   theEvent.returnValue = false;
   if(theEvent.preventDefault) theEvent.preventDefault();
   }
   }
</script>


<style type="text/css">
  .disabledbutton {
    pointer-events: none;
    opacity: 0.4;
}




</style>
  @endsection



