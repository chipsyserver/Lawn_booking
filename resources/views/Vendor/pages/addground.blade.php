 @extends('Vendor.layouts.master_layout')
@section('content')
 <div class="page">
    <div class="page-header">
      <h1 class="page-title"> Lawn Details</h1>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/vendor/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Lawn Details</li>
      </ol>
  
    </div>

 


  <div class="page-content container-fluid" ng-controller="GoundController" ng-cloak>

      <!-- /.row -->
     <!--  <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                 <div class="header form-inline">
               <h2>
                            <span>Total Ground( <%totalUsers%> )</span>
                            <p class="pull-right">
                              

                                 <a href="/superadmin/ground-create" class="btn btn-info " id="submit">Add New Ground</a>

                            </p>
                        </h2>
              </div> -->
<!-- 
              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <a href="/superadmin/ground-create" class="btn btn-info pull-right" id="submit">Add New Ground</a>
                  </div>
                </div> -->
             <div class="col-md-12" id="editdisable">
       <div class="panel">
          <div class="panel-body container-fluid">
        
       <div class="example-wrap">
                <h4 class="example-title">Total Ground ( <%totalUsers%> )</h4>

         
            <!-- /.box-header -->
             <div class="example table-responsive">
                  <table class="table">
                <tbody>
                <tr>
                  <th>ID</th>
                  <th>Ground Name</th>
                  <th>Latitude</th>
                  <th>Longitude</th>
                  <th>Vendor Name</th>
   
                  <th>Price Per Day</th>
                  
                  
                  <th >Action</th>
                </tr>
               
                <tr ng-repeat="ground in grounds" ng-cloak>
                                  <th scope="row"><%(usersPerPage * (currentPage-1)) + $index+1  %></th>
                                   <td  title="<% ground.title %>"><% ground.title %> </td>
                                                      <td><% ground.latitude %></td>
                                   <td><% ground.longitude %></td>
                                   <td><% ground.username %></td>
                                   
                             
                                   <td> <% ground.priceperday %></td>
                                                             
                                    
                                    
                                    <td style="width: 10px;"> 

            <a href="/vendor/lawn/edit/<%ground.id%>" class="btn btn-icon btn-info waves-effect waves-classic">Book
                        </a>



                                    </td>
                                    
                                </tr>
                                    <tr ng-hide="grounds.length">
                                   <td colspan="11" align="center" style="color: red"><b>No Ground Details to display</b></td>
                                </tr> 
              </tbody></table>
              
            </div>
            <div class="box-footer clearfix">
             <div class="col-md-6 text-right">
                            <ul uib-pagination total-items="totalUsers" ng-model="currentPage" class="pagination pagination-sm" boundary-links="true" rotate="false" max-size="maxSize" items-per-page="usersPerPage"></ul>
                          </div>
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
</div>
</div>
</div>



<script src="{{asset('backend/jscontroller/book.js')}}"></script>

  @endsection