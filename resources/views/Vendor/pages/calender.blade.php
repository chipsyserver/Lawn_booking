
<link rel="stylesheet" type="text/css" href="{{asset('admin/parsley/parsley.css')}}">




<meta charset='utf-8' />
<link href='/calender/fullcalendar.min.css' rel='stylesheet' />
<link href='/calender/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='/calender/lib/moment.min.js'></script>
<script src='/calender/lib/jquery.min.js'></script>
<script src='/calender/fullcalendar.min.js'></script>
<script>


  $(document).ready(function() {

    $('#calendar').fullCalendar({
      // header: {
      //   left: 'prev,next today',
      //   center: 'title',
      //   right: 'month,agendaWeek,agendaDay,listWeek'
      // },
      defaultDate: '2019-01-01',
      editable: true,
       selectable: true,
      selectHelper: true,
      select: function(start, end) {
        // var title = prompt('Event Title:');
        var eventData;
        alert(start);
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },
      navLinks: true, // can click day/week names to navigate views
      eventLimit: true, // allow "more" link when too many events
      events: {
        url: '/vendor/lawn/booking_details/6',
        error: function() {
          $('#script-warning').show();
        }
      },
      loading: function(bool) {
        $('#loading').toggle(bool);
      }
    });

  });

 

</script>
<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }

</style>
</head>

<body>

      
  <div id='calendar'></div>

</body>

