<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>Dashboard | Vendor</title>

  <link rel="apple-touch-icon" href="/backend/assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="/backend/assets/images/favicon.ico">

 <link rel="stylesheet" href="/backend/css/bootstrap.min.css">
  <link rel="stylesheet" href="/backend/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="/backend/assets/css/site.min.css">

  <!-- Skin tools (demo site only) -->
<!--   <link rel="stylesheet" href="/backend/css/skintools.min.css">
  <script src="/backend/assets/js/Plugin/skintools.min.js"></script> -->

  <!-- Plugins -->
  <link rel="stylesheet" href="/backend/vendor/animsition/animsition.min.css">
  <link rel="stylesheet" href="/backend/vendor/asscrollable/asScrollable.min.css">
  <link rel="stylesheet" href="/backend/vendor/switchery/switchery.min.css">
  <link rel="stylesheet" href="/backend/vendor/intro-js/introjs.min.css">
  <link rel="stylesheet" href="/backend/vendor/slidepanel/slidePanel.min.css">
  <link rel="stylesheet" href="/backend/vendor/flag-icon-css/flag-icon.min.css">
  <link rel="stylesheet" href="/backend/vendor/waves/waves.min.css">
 <link rel="stylesheet" href="/backend/vendor/formvalidation/formValidation.min.css">
  <link rel="stylesheet" href="/backend/vendor/blueimp-file-upload/jquery.fileupload.min.css">

    <link rel="stylesheet" href="/backend/vendor/dropify/dropify.min.css">
  <!-- Page -->
  <link rel="stylesheet" href="/backend/assets/examples/css/forms/validation.min.css">
  <!-- Plugins For This Page -->
  <link rel="stylesheet" href="/backend/vendor/chartist/chartist.min.css">
  <link rel="stylesheet" href="/backend/vendor/jvectormap/jquery-jvectormap.min.css">
  <link rel="stylesheet" href="/backend/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.css">
 <!--  <link rel="stylesheet" type="text/css" href="/backend/backend/assets/bootstrap/css/bootstrap.css""> -->
  <link rel="stylesheet" href="/backend/vendor/fullcalendar/fullcalendar.min.css">
    <link rel="stylesheet" href="/backend/assets/examples/css/apps/calendar.min.css">
  <!-- Page -->
  <link rel="stylesheet" href="/backend/assets/examples/css/dashboard/v1.min.css">
  <link rel="stylesheet" href="/backend/assets/examples/css/structure/pagination.min.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="/backend/fonts/material-design/material-design.min.css">
  <link rel="stylesheet" href="/backend/fonts/brand-icons/brand-icons.min.css">
 <link href="/backend/assets/sweetalert/sweetalert.css" rel="stylesheet" type="text/css"><!-- 
      <link href="/assests/jquery-confirm/jquery-confirm.css" rel="stylesheet" type="text/css">
 -->

    
  <link rel='stylesheet' href="https://fonts.googleapis.com/css?family=Roboto:400,400italic,700">
  <script src="/backend/vendor/breakpoints/breakpoints.min.js"></script>
  <script>
    Breakpoints();
  </script>

         <script src="/backend/assets/js/jquery.min.js"></script>
 
       
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script src="{{asset('/backend/assets/angular/angular.js')}}"></script>
    <script src="{{asset('/backend/assets/angular/route.js')}}"></script>
    <script src="{{asset('/backend/assets/angular/ui-bootstrap-tpls-2.5.0.min.js')}}"></script>


</head>