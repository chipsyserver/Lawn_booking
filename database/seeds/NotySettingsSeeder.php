<?php

use Illuminate\Database\Seeder;

class NotySettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Dummy Entries starts
        $settings = DB::collection('noty_settings')->where('status', 1)->get();
        $users = DB::collection('users')->get();

        foreach ($users as $value) {
            foreach ($settings as $sett) {
                DB::collection('user_notifications')->insert([
                    'user_id'      => (string)$value['_id'],
                    'noty_setting_id'  => (string)$sett['_id'],
                    'checked'       => 1
                ]);
            }
        }
        // Dummy entires ends
        
        // DB::table('noty_settings')->insert([
        //     'description' => 'Receive message from administrator',
        //     'status' => 1,
        // ]);
        
        // DB::table('noty_settings')->insert([
        //     'description' => 'Others liked your post',
        //     'status' => 1,
        // ]);

        // DB::table('noty_settings')->insert([
        //     'description' => 'Others commented on your post',
        //     'status' => 1,
        // ]);

        // DB::table('noty_settings')->insert([
        //     'description' => 'Others shared your post',
        //     'status' => 1,
        // ]);
    }
}
