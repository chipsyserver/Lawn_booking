<?php

use Illuminate\Database\Seeder;

class SkillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $skills = [
            'Android Developer Skills',
            'Architect Skills',
            'Big Data Skills',
            'Biomedical Engineer Skills',
            'Civil Engineer Skills',
            'Cloud Computing Skills',
            'Computer Skills',
            'Content Management Skills',
            'Computer Programming Skills',
            'Computer Systems Analyst Skills',
            'Data Analyst Skills',
            'Data Scientist Skills',
            'Database Administrator (DBA) Skills',
            'Digital Media Skills',
            'Engineering Skills',
            'Front End Web Developer Skills',
            'Graphic Design Skills',
            'Information Security Analyst Skills',
            'Information Technology Skills',
            'iOS Developer Skills',
            'IT Manager Skills',
            'LinkedIn Skills',
            'Marketing Automation Specialist/Manager Skills',
            'Market Research Analyst Skills',
            'Mechanical Engineer Skills',
            'Problem Solving Skills',
            'Product Manager Skills',
            'Project Manager Skills',
            'Research Skills',
            'Scrum Master Skills',
            'Search Engine Optimization (SEO)',
            'Software Developer Skills',
            'Software Engineer Skills',
            'Software Quality Assurance (QA) Engineer Skills',
            'Team Building Skills',
            'Tech Support Skills',
            'Technical Support Engineer Skills',
            'Technical Writer Skills',
            'UI / UX',
            'Web Design'
        ];

        foreach ($skills as $skill) {
            DB::table('skills')->insert([
                'skill' => $skill,
                'area' => 1, // Technical,
                'status'=>1 // Active
            ]);
        }
    }
}
