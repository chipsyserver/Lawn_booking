<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            "roll_id" => '100',
            "user_type_id" => '203',
            "email" => 'admin',
            "password" => '$2y$10$NoC0xHDR0Jm.Vd5PZN18IOWRRLBGHdMJDSoThFt5uluRNd7mUFh2q',
            "mobile_number" => '9066047177',
            "first_name" => 'Praveen',
            "last_name" => 'Ameen',
            "remember_token" => 'YhaqpFWqZdm7Xmba6SWoqtQKdPAK3CuMGoN9bHoYzOgrV7OaNwRZcDNMtlyK',
            "profile_img" => 'default_user.png',
            "expires_in" => 0,
            "linkedin_id" => 'null',
            "active_state" => '0',
            "sink_state" => '0',
            "name" => 'null',
            "avatar" => 'null',
            "headline" => 'null',
            "logby" => 'web',
            "industry" => 'null',
            "public_profile_url" => 'null',
            "avatar_original" => 'null',
            "address" => 'tgre kjf ks',
            "gender" => 'male',
            "first_login" => 1,
            "dob" => 658886400,
            "last_signin" => 0,
            "created" => 1492217675
        ]);
        
        DB::table('users')->insert([
            "roll_id" => '2',
            "user_type_id" => '203',
            "email" => 'praslbaikady@gmail.com',
            "password" => '$2y$10$sya8GwhoO7/ij6oZP64ulOOmi0dHc0/3XHcMtTmNUIk.A9WsLMooa',
            "mobile_number" => '9066047177',
            "first_name" => 'Praveen',
            "last_name" => 'Ameen',
            "remember_token" => 'dY8v3SPh5k5em2lIYOctIZhgpgu4I5IMwjCtQ4ie2sbgiNZ14aIb4ffAbAxX',
            "profile_img" => '20170427094358.png',
            "expires_in" => 0,
            "linkedin_id" => 'null',
            "active_state" => '100',
            "sink_state" => '0',
            "name" => 'null',
            "avatar" => 'null',
            "headline" => 'null',
            "logby" => 'web',
            "industry" => 'null',
            "public_profile_url" => 'null',
            "avatar_original" => 'null',
            "address" => 'Gandhinagar Baikady',
            "gender" => 'male' ,
            "first_login" => 1,
            "dob" => 658886400,
            "last_signin" => 0,
            "created" => 1492736075
        ]);
    }
}
