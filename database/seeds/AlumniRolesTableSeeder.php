<?php

use Illuminate\Database\Seeder;

class AlumniRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Guest Lecture',
            'Mentoring Juniors'
        ];

        foreach ($roles as $role) {
            DB::table('alumni_roles')->insert([
                'role' => $role,
                'status'=>1 // Active
            ]);
        }
    }
}
