<?php

use Illuminate\Database\Seeder;

class SocialNetworksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('social_networks')->insert([
            'name' => 'Facebook',
            'status' => 1,
        ]);
        
        DB::table('social_networks')->insert([
            'name' => 'Twitter',
            'status' => 1,
        ]);

        DB::table('social_networks')->insert([
            'name' => 'Google Plus',
            'status' => 1,
        ]);

        DB::table('social_networks')->insert([
            'name' => 'LinkedIn',
            'status' => 1,
        ]);
    }
}
