<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        // $this->call(SocialNetworksSeeder::class);
        // $this->call(UsersSeeder::class);
        // $this->call(SkillsSeeder::class);
        // $this->call(PrivacySettingsSeeder::class);
        $this->call(NotySettingsSeeder::class);
    }
}
