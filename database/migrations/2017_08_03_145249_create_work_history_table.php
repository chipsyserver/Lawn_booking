<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('work_history', function (Blueprint $table) {
             $table->increments('id');
             $table->string('user_id');
             $table->string('company_name');
             $table->string('position');
             $table->string('city');
             $table->string('starting_time');
             $table->string('ending_time');
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_history');
    }
}
