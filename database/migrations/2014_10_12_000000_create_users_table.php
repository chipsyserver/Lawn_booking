<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('roll_id');
            $table->string('user_type_id');
            $table->string('name');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('headline');
            $table->string('email')->unique();
            $table->string('linkedin_email');
            $table->string('password');
            $table->string('mobile_number');
            $table->string('linkedin_id');
            $table->string('public_profile_url');
            $table->string('avatar_original');
            $table->string('avatar');
            $table->string('profile_img');
            $table->string('cover_image');
            $table->string('cover_image_pos');
            $table->string('gender');
            $table->integer('dob');
            $table->string('industry');
            $table->string('address');
            $table->string('logby');
            $table->string('want_to_be');
            $table->rememberToken();
            $table->string('active_state');
            $table->string('sink_state');
            $table->integer('first_login');
            $table->integer('expires_in');
            $table->integer('last_signin');
            $table->integer('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
