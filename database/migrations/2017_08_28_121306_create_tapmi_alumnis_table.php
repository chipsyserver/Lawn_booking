<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTapmiAlumnisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tapmi_alumnis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rno');
            $table->string('ini');
            $table->string('name');
            $table->string('dob');
            $table->string('father_name');
            $table->string('organisation');
            $table->string('designation');
            $table->string('address1');
            $table->string('address2');
            $table->string('address3');
            $table->string('address4');
            $table->string('location');
            $table->string('state');
            $table->string('pin');
            $table->string('telephone');
            $table->string('mobile');
            $table->string('email1');
            $table->string('email2');
            $table->string('year');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tapmi_alumnis');
    }
}
