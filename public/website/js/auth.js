var url="";
$.auth = {
    init: function(callback) {
        $(document).on('click', '[name="booking_lawn"]', function() {
        var log=$('#is_login').val();
        if(log==1){
            $.auth.form_booking_validate($(this), callback);
        }else{
           url=window.location.href+'?date_of_booking='+$('[name=date_of_booking]').val()+'&desc='+$('[name=desc]').val();
             
           $('.modal-backdrop').hide(); 
           $('body').removeClass('modal-open'); 
           $('#createLawn').modal('hide');
           $('#loginModal').modal('toggle');
        }
        });

         $(document).on('click', '[name="book_now"]', function() {
           $('#datepicker').val('');
           $('#desc').val('');
           $('.signup-error').html('');
        });

        $(document).on('click', '[name="create_account"]', function() {
            
            $.auth.form_validate($(this), callback);
        });


        $(document).on('click', '#verify_otp', function() {
            $.auth.otpVerify();
        });

        $(document).on('click', '#resend_otp', function() {
            $.auth.resendOtp();
        });
    },
    form_booking_validate: function(submitbutton) {
        var form = submitbutton.parents('form');
        var proceed = form.parsley().validate();
        if (proceed) $.auth.form_booking_submit(form);
    },
    form_booking_submit: function(form) {
        uploadObject = form.ajaxSubmit({
            beforeSubmit: function() {
                // $.loader.start();
            },
            uploadProgress: function(event, position, total, percentComplete) {

            },
            success: function(response) {
                // $.loader.stop();
                if (response.success) {
                     var price=parseInt($('[name=price_amt]').val());
                     window.location.href = '/pay-now/'+price;
                     form.find('.signup-error').html(response.message).css('color', 'green');
                     setTimeout(function(){ form.find('.signup-error').html('');

                        $('.modal-backdrop').hide(); // for black background
                        $('body').removeClass('modal-open'); // For scroll run
                        $('#createLawn').modal('hide'); }, 3000);
                        $('#datepicker').val('');
                        $('#desc').val('');

                        
                } else {
                    //console.log(response);
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    // console.log(display);
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            },
            error: function() {
                alert('error');
                // $.loader.stop();
                // $.confirm({
                //     title: 'Error!',
                //     content: response.message,
                //     buttons: {
                //         OK: function() {

                //         },

                //     }
                // });
            },
        });
    },

    form_validate: function(submitbutton) {
        var form = submitbutton.parents('form');
        var proceed = form.parsley().validate();
        if (proceed) $.auth.form_submit(form);
    },

    form_submit: function(form) {
        uploadObject = form.ajaxSubmit({
            beforeSubmit: function() {
                // $.loader.start();
            },
            uploadProgress: function(event, position, total, percentComplete) {

            },
            success: function(response) {
                // $.loader.stop();
                if (response.success) {
                    console.log(response);
                    var append='';
                    if (response.status==1){
                        append+='<div class="form-group"><div class="ui-input-group">'+
                                '<input required type="text" name="full_name" class="form-control" placeholder="">'+
                                '<span class="input-bar"></span><label>Name</label></div></div>'+
                                '<div class="form-group"><div class="ui-input-group">'+
                                '<input  type="email" name="email" class="form-control" placeholder="">'+
                                '<span class="input-bar"></span><label>Email</label></div></div>';

                    }
                    var modal_content = ' <div class="modal-header" id="otp">'+
                     '<h5 class="modal-title" id="exampleModalLabel">Enter One Time Password (OTP)</h3>'+
                    ' <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'+
                    '<div class="modal-body">'+
                    '<p class="text-center" id="resp-msg"></p>'+
                    '<p>OTP has been sent to your mobile('+response.mobile+')</p>'+
                    '<form name="otp_verify" class="form-horizontal ng-pristine ng-valid" method="post" role="form">'+
                    '<fieldset> <div class="form-group">  <div class="ui-input-group">'+
                    '<input type="text" name="otp" required class="form-control" placeholder="" data-parsley-required-message="Enter OTP">'+
                    ' <span class="input-bar"></span> <label>OTP</label></div></div>'+append+
                    '<div class="form-group formField" style="text-align: center;">'+
                    '<input type="button" class="btn btn-primary btn-block bg-color-3 border-color-3" id="verify_otp" value="Verify" style="    width: 200px;">'+
                    '</div>'+
                    '<div class="form-group formField">'+
                    '<p class="help-block text-right"> <span style="cursor:pointer;color:blue" id="resend_otp">Resend OTP</span></p>'+
                    '</div>'+
                    '</form>'+
                    '</div>'+
                    '</div>';
                    $('#createAccount').modal('hide');
                     $('#loginModal').modal('hide');
                    $('#otp_verify_mod').find('.modal-dialog').addClass('otp-modal');
                    $('#otp_verify_mod .modal-content').html(modal_content);
                    $('#otp_verify_mod').modal('show');
                } else {
                    console.log(response);
                    var display = "";
                    if ((typeof response.message) == 'object') {
                        $.each(response.message, function(key, value) {
                            display = value[0];
                        });
                    } else {
                        display = response.message;
                    }
                    // console.log(display);
                    form.find('.signup-error').html('* '+display).css('color', 'red');
                    // $('[name=user-register] input').val('');
                }
            },
            error: function() {
                alert('error');
                // $.loader.stop();
                // $.confirm({
                //     title: 'Error!',
                //     content: response.message,
                //     buttons: {
                //         OK: function() {

                //         },

                //     }
                // });
            },
        });
    },
    resendOtp : function() {
        $.get('/resend-otp', function(response) {
            if (response.success) {
                $('#resp-msg').removeClass('text-danger').addClass('text-success').text(response.message);
            }
        })
    },
    otpVerify : function() {
        var proceed = $('[name=otp_verify]').parsley().validate();
         if (proceed) {
            var postdata = {};
            var data = $('[name=otp_verify]').serializeArray();
            $.each(data, function() {
                postdata[this.name] = this.value || '';
            });
            $.post('/verify-otp', postdata, function(response) {
                
                if (response.success) {
                    // location.href =  response.redirect;
                   
                    if(typeof(url) != "undefined" && url !== null) {
                       location.href = url;
                    }else location.reload();
                    
                    $('#quick-shop-modal').find('.modal-dialog').removeClass('otp-modal');
                    $('#quick-shop-modal .modal-body').empty();
                    $('#quick-shop-modal').modal('hide');
                   
                } else {
                    $('#resp-msg').removeClass('text-success').addClass('text-danger').text(response.message);
                }
            })
        } else {
            $('.clear-fix').html('');
        }
    }
}
$.auth.init();