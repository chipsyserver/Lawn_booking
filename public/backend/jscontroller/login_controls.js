 admin.controller("LoginController", ['$scope', function($scope) { 

    $scope.submitLogin = function() {
        var proceed = true;
            // $('[name=login-inputs] [required=true]').filter(function() {
            //     if ($.trim(this.value) == "") {
            //         $(this).closest('input').css('border-color', 'red');
            //         proceed = false;
            //     } 
            //   });
             proceed = $('[name=login-inputs]').parsley().validate();
            if(proceed) {
                    var postdata = { "username": $('[name=login-inputs] [name=username]').val(),
                       "password": $('[name=login-inputs] [name=password]').val(),
                     };
                   
	                  $.post('/superadmin/auth-login',postdata,function(response){         
		                    if (response.success) {  location.href = "/superadmin/dashboard"; } 
			                  else {
				                var display = "";
                        if ((typeof response.message) == 'object') {
                            $.each(response.message, function(key, value) {
                                display = value[0];
                            });
                        } else {
                            display = response.message;
                        }
                        $('[name=login-inputs] [name=message_area]').html(display).css('color', 'red');
                        $('[name=login-inputs] [name=password]').val('');
      		        }
      			},'json'); 
           }
    }

    $scope.clear = function(ths) {     
            $('[name=message_area]').html('');
            ths.css('border-color', '');
    }
    
   
   
    //Insert click events  
   $(document).on('keyup','input', function(e) {  
     var code = e.which; 
     if(code==13)e.preventDefault();
     if(code==32||code==13||code==188||code==186){ 
        $('[name=login-inputs] [name=submit-login]').click();      
      }else{
       $(this).css('border-color', ''); 
        $scope.clear(this); }
    });
   //
    $(document).on('click', '[name=login-inputs] [name=submit-login]', function() {   
   $scope.submitLogin();
    });
}]);
