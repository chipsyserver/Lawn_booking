var myApp = angular.module('lawn', ['ngRoute', 'ui.bootstrap'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>')
});

myApp.controller("GoundController", ['$scope', '$http', function($scope, $http) {

$(document).on('click', '#btnRowcancel', function(){

    $(this).parents("tr").remove();
});





$("[name=update]").click(function() {

    var id=$(this).attr('id');
    var proceed = $('[name=ground]').parsley().validate();
    if (proceed) {
           swal({   
                title: "Are you sure?",   
                text: "You will  be able to Update  Ground Details",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, update it!",   
                closeOnConfirm: false 
            }, function(){
    $("div#divLoading").addClass('show');
    var formData = new FormData($('[name=ground]')[0]);
    $.ajax({
                                type: "POST",
                                url: "/vendor/lawn/update/"+id,
                                data: formData,
                                async: false,
                                cache: false,
                                contentType: false,
                                processData: false,


                                success: function(response) {
                              
                                     $("div#divLoading").removeClass('show');
                                    if (response.success) {


                                      
                                         swal('Success!', response.message, 'success');

                                    } else {
                                        $("div#divLoading").removeClass('show');
                                        swal('Error!',response.message, 'error');
                                    }

                                     
                                },
                                error: function(response) {

                                    swal('Error!', 'Something went wrong', 'error');
                                },
                            });


     });

    }

        
    });





   $("#submit").click(function() {


        var proceed = $('[name=ground]').parsley().validate();
     var files = $('input[name=ground_img]').files;
     
        if (proceed) {
      
        $("div#divLoading").addClass('show');

       
                    var formData = new FormData($('[name=ground]')[0]);
                            $.ajax({
                                type: "POST",
                                url: "/superadmin/ground/add",
                                data: formData,
                                async: false,
                                cache: false,
                                contentType: false,
                                processData: false,


                                success: function(response) {
                             
                                     $("div#divLoading").removeClass('show');
                                    if (response.success) {


                                        // $.alert({
                                        //     title: 'Success!',
                                        //     content: 'Details updated Successfully',
                                        //     icon: 'fa fa-rocket',
                                        //     animation: 'zoom',
                                        //     closeAnimation: 'zoom',
                                        //     buttons: {
                                        //         ok: {
                                        //             text: 'ok',
                                        //             btnClass: 'btn-blue',
                                        //             action: function() {
                                        //                 window.location = "/superadmin/ground-detailes";

                                        //             }
                                        //         }
                                        //     }
                                        // });
                                         swal('Success!', response.message, 'success');

                                    } else {
                                        $("div#divLoading").removeClass('show');
                                        swal('Error!',response.message, 'error');
                                    }

                                     
                                },
                                error: function(response) {

                                    swal('Error!', 'Something went wrong', 'error');
                                },
                            });

    }

        
    });
$scope.delete = function(event) {
        var id = angular.element(event.currentTarget).attr('id');
 
      swal({   
                title: "Are you sure?",   
                text: "You Want Delete Category",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",   
                confirmButtonText: "Yes, delete it!",   
                closeOnConfirm: false 
            }, function(){
                       $("div#divLoading").addClass('show');

                        $.ajax({
                            url: '/superadmin/ground/delete/' + id,
                            type: 'DELETE',
                            success: function(response) {
                             
                              $("div#divLoading").removeClass('show');
                                if (response.success) {

                                //$('#'+id).closest('tr').hide();
                                   setTimeout(function() {
                              $('#'+id).closest('tr').css('background-color', 'rgb(181, 117, 117)').hide(1000); 
                            }, 1001);
                            // display = response.message;
                            display = "Ground has been deleted.";

                            swal("Success!", display, "success");
                                }
                                else{
                                     swal("Success!","Some Thing Happens", "success");
                                }


                            }


                        });
                           
                         });

           

    }




       $scope.maxSize = 4;

    $scope.$watch("currentPage", function() {


        $scope.getVisitors($scope.currentPage);
    });

    //load Visitors     
    $scope.getVisitors = function(page = 1) {

       $("div#divLoading").addClass('show');
        $http.get('/vendor/lawn/display-ground-detailes?page=' + page)
            .then(function(res) {

                $("div#divLoading").removeClass('show');
                $scope.grounds = res.data.data;

                $scope.totalUsers = res.data.total;
                $scope.currentPage = res.data.current_page;
                $scope.usersPerPage = res.data.per_page;
                $scope.usersFrom = res.data.from ? res.data.from : 0;
                $scope.usersTo = res.data.to ? res.data.to : 0;
            });
    }
    $scope.getVisitors(1);

   

}]);